<?php

namespace App\DataFixtures\ORM;


use App\Entity\Address;
use App\Entity\Drive;
use App\Entity\DriveStatus;
use App\Entity\DriveType;
use App\Entity\Seat;
use App\Entity\SeatStatus;
use App\Entity\User;
use App\Entity\UserSettings;
use App\Entity\Vehicle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{

//User
    private function getUser1()
    {
        return new User(
            'user1@gamil.com',
            'Simon',
            'Phipps',
            'user1',
            'http://hackathon.bz.it/image/journal/article?img_id=201627&t=1509705312757'
        );
    }
    private function getUser2()
    {
        return new User(
            'user3@gamil.com',
            'Xiaofeng',
            'Wang',
            'user3',
            'http://hackathon.bz.it/image/journal/article?img_id=201680&t=1509986471554'
        );
    }
    private function getUser3()
    {
        return new User(
            'user2@gamil.com',
            'Patrizia',
            'Del Tedesco',
            'user2',
            'http://hackathon.bz.it/image/journal/article?img_id=201693&t=1509986887357'
        );
        
    }

//Vehicle
    private function getVehicle1($user)
    {
        return new Vehicle('Vehicle1', $user);
    }
    private function getVehicle2($user)
    {
        return new Vehicle('Vehicle2', $user);
    }
    private function getVehicle3($user)
    {
        return new Vehicle('Vehicle3', $user);
    }

//Address
    private function getAddressUser1Home($user)
    {
        return new Address('Home', 'Terlan', 39018, 'Via Silberleiten 34', $user);
    }
    private function getAddressUser1Work($user)
    {
        return new Address('Work', 'Bozen', 39100, 'Via Claudia Augusta 73', $user);
    }
    private function getAddressUser2Home($user)
    {
        return new Address('Home', 'Siebeneich', 39018, 'Via Mondschein 1', $user);
    }
    private function getAddressUser2Work($user)
    {
        return new Address('Work', 'Lana', 39011, 'Via Monte Cervina 3', $user);
    }
    private function getAddressUser3Home($user)
    {
        return new Address('Home', 'Terlan', 39018, 'Via Wolfsthurn 6', $user);
    }
    private function getAddressUser3Work($user)
    {
        return new Address('Work', 'Bozen', 39100, 'Vicolo del Lauro 1', $user);
    }



//DriverType
    private function getDriveTypeDriver()
    {
        return new DriveType(10, 'driver');
    }

    private function getDriveTypePassenger()
    {
        return new DriveType(20, 'passenger');
    }


//SeatStatus
    private function getSeatStatusRequested()
    {
        return new SeatStatus(10, 'requested');
    }

    private function getSeatStatusAccepted()
    {
        return new SeatStatus(20, 'accepted');
    }

    private function getSeatStatusDeclined()
    {
        return new SeatStatus(30, 'declined');
    }

    private function getSeatStatusCanceled()
    {
        return new SeatStatus(40, 'canceled');
    }


//DriveStatus
    private function getDriveStatusPending()
    {
        return new DriveStatus(10, 'pending');
    }

    private function getDriveStatusDriving()
    {
        return new DriveStatus(20, 'driving');
    }

    private function getDriveStatusFinished()
    {
        return new DriveStatus(30, 'finished');
    }
    
    public function load(ObjectManager $manager)
    {
        $user1 = $this->getUser1();
        $manager->persist($user1);
        $manager->persist($this->getVehicle1($user1));
        $manager->persist($this->getAddressUser1Home($user1));
        $manager->persist($this->getAddressUser1Work($user1));
        
        $user2 = $this->getUser2();
        $manager->persist($user2);
        $manager->persist($this->getVehicle2($user2));
        $manager->persist($this->getAddressUser2Home($user2));
        $manager->persist($this->getAddressUser2Work($user2));
        
        $user3 = $this->getUser3();
        $manager->persist($user3);
        $manager->persist($this->getVehicle3($user3));
        $manager->persist($this->getAddressUser3Home($user3));
        $manager->persist($this->getAddressUser3Work($user3));

        $manager->persist($this->getDriveTypeDriver());
        $manager->persist($this->getDriveTypePassenger());

        $manager->persist($this->getSeatStatusRequested());
        $manager->persist($this->getSeatStatusAccepted());
        $manager->persist($this->getSeatStatusDeclined());
        $manager->persist($this->getSeatStatusCanceled());

        $manager->persist($this->getDriveStatusPending());
        $manager->persist($this->getDriveStatusDriving());
        $manager->persist($this->getDriveStatusFinished());

        $manager->flush();
    }
}
