<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Address;
use App\Entity\Drive;
use App\Entity\DriveRequest;
use App\Entity\DriveStatus;
use App\Entity\DriveType;
use App\Entity\Seat;
use App\Entity\User;
use App\Utils\GoogleMaps;

class DriveController extends Controller
{
    private function getMyCurrentDrive($user)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT d
            FROM App:Drive d
            WHERE d.user = :user AND d.closed = FALSE
            ORDER BY d.timestamp DESC'
        )
        ->setParameter('user', $user->getId());
        $query->setMaxResults(1);
        return $query->getOneOrNullResult();
    }

    private function getMatches($drive)
    {
        // $googleMaps = new GoogleMaps();

        // add tolerance of 5 minutes
        $date = $drive->getTimestampOfDeparture();
        $date->add(new \DateInterval(sprintf("PT%dM", 5)));

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT d
            FROM App:Drive d
            JOIN d.departure dep
            JOIN d.arrival arr
            WHERE d.type <> :type
            AND dep.zipCode = :departure AND arr.zipCode = :arrival'
        )
        //    AND CURRENT_TIMESTAMP() <= d.timestampOfDeparture
        //    AND d.timestampOfDeparture < :myTimestampOfDeparture'
        //)
            ->setParameter('type', $drive->getType()->getId())
            // TODO: departure and arrival should be replace by a algorithm,
            //  which matches the most common ride (maybe 80%)
            ->setParameter('departure', $drive->getDeparture()->getZipCode())
            ->setParameter('arrival', $drive->getArrival()->getZipCode())
            //->setParameter('myTimestampOfDeparture', $date)
        ;
        $matches = $query->getResult();
        foreach ($matches as $match) {
            if ($drive->isDriver()) {
                /*$route = $googleMaps->getRoute(
                    $drive->getStartLocation(),
                    $drive->getEndLocation(),
                    $match
                );*/

                $driveRequest = $this->getDoctrine()
                ->getRepository(DriveRequest::class)
                ->findOneBy([
                    'driver' => $drive->getId(),
                    'passenger' => $match->getId()
                ]);
            } else {
                /*$route = $googleMaps->getRoute(
                    $match->getStartLocation(),
                    $match->getEndLocation(),
                    $drive
                );*/
                
                $driveRequest = $this->getDoctrine()
                ->getRepository(DriveRequest::class)
                ->findOneBy([
                    'driver' => $match->getId(),
                    'passenger' => $drive->getId()
                ]);
            }
            if (/*is_null($route) || */ !is_null($driveRequest)) {
                continue;
            }
            
            if ($drive->isDriver()) {
                $driveRequest = new DriveRequest($drive, $match);
            } else {
                $driveRequest = new DriveRequest($match, $drive);
            }
            $em->persist($driveRequest);
            $em->flush();
        }

        if ($drive->isDriver()) {
            $matches = $this->getDoctrine()
            ->getRepository(DriveRequest::class)
            ->findByDriver($drive->getId());
        } else {
            $matches = $this->getDoctrine()
            ->getRepository(DriveRequest::class)
            ->findByPassenger($drive->getId());
        }

        return $matches;
    }

    /**
     * @Route("/drive")
     * @Method({"OPTIONS", "PUT", "POST", "GET"})
     */
    public function getDriveAction(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        ];
        if ($request->getMethod() === 'OPTIONS') {
            $response = new JsonResponse([], JsonResponse::HTTP_OK, $headers);
            return $response;
        }
        $userId = $request->headers->get('Authorization');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $drive = $this->getMyCurrentDrive($user);

        if ($request->getMethod() === 'POST' && is_null($drive)) {
            $input = json_decode($request->getContent(), true);
            $departure = $this->getDoctrine()
                ->getRepository(Address::class)
                ->find($input['departureId']);
            $arrival = $this->getDoctrine()
                ->getRepository(Address::class)
                ->find($input['arrivalId']);
            $driveType = $this->getDoctrine()
                ->getRepository(DriveType::class)
                ->find($input['driveTypeId']);
            $driveStatus = $this->getDoctrine()
                ->getRepository(DriveStatus::class)
                ->find(10);

            $timestampOfDeparture = new \DateTime();
            $timestampOfDeparture->add(new \DateInterval(sprintf("PT%dM", $input['minutesToStart'])));

            $drive = new Drive(
                $user,
                new \DateTime(),
                $departure,
                $arrival,
                $input['minutesToStart'],
                $timestampOfDeparture,
                isset($input['seating']) ? $input['seating'] : null,
                $driveType,
                $driveStatus
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($drive);
            $em->flush();
        } elseif ($request->getMethod() === 'PUT') {
            if (!$drive->isDriver()) {
                $response = new JsonResponse(['error' => 'forbidden'], JsonResponse::HTTP_FORBIDDEN, $headers);
                return $response;
            }
    
    
            $seats = $em->getRepository(Seat::class)->findByDrive($drive->getId());
            foreach ($seats as $seat) {
                $passengerDrive = $this->getMyCurrentDrive($seat->getUser());
                $passengerDrive->setClosed();
                $em->persist($passengerDrive);
            }
            
            $drive->setClosed();
            $em->persist($drive);
            $em->flush();
        }
            
        $data = [
            'data' => $drive
        ];

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new JsonResponse($json, JsonResponse::HTTP_OK, $headers, true);
        return $response;
    }

    /**
     * @Route("/drive/matches")
     * @Method({"OPTIONS", "GET"})
     */
    public function getMatchesAction(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        ];
        if ($request->getMethod() === 'OPTIONS') {
            $response = new JsonResponse([], JsonResponse::HTTP_OK, $headers);
            return $response;
        }
        $userId = $request->headers->get('Authorization');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $drive = $this->getMyCurrentDrive($user);
        $matches = $this->getMatches($drive);
            
        $data = [
            'data' => $matches
        ];

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new JsonResponse($json, JsonResponse::HTTP_OK, $headers, true);
        return $response;
    }

    private function checkSeats($drive)
    {
        $em = $this->getDoctrine()->getManager();
        $seats = $em->getRepository(Seat::class)->findByDrive($drive->getId());
        if (count($seats) >= $drive->getSeating()) {
            throw new \Exception('Something went wrong!');
        }
    }

    /**
     * @Route("/seat")
     * @Method({"OPTIONS", "POST"})
     */
    public function handleSeatAction(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
        ];
        if ($request->getMethod() === 'OPTIONS') {
            $response = new JsonResponse([], JsonResponse::HTTP_OK, $headers);
            return $response;
        }

        $em = $this->getDoctrine()->getManager();
        $userId = $request->headers->get('Authorization');
        $input = json_decode($request->getContent(), true);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $drive = $this->getMyCurrentDrive($user);
        
        $hasChanged = false;
        $data = [
            'data' => []
        ];
        $matches = $this->getMatches($drive);
        foreach ($matches as $match) {
            if ($drive->isDriver()) {
                if ($match->getPassenger()->getUser()->getId() === $input['userId']) {
                    $hasChanged = true;
                    $match->setDriverChecked();
                }
            } else {
                if ($match->getDriver()->getUser()->getId() === $input['userId']) {
                    $hasChanged = true;
                    $match->setPassengerChecked();
                }
            }

            if ($hasChanged) {
                $em->persist($match);
    
                if ($match->getDriverChecked() && $match->getPassengerChecked()) {
                    if ($drive->isDriver()) {
                        $this->checkSeats($drive);
                        $seat = new Seat($drive, $match->getPassenger()->getUser());
                    } else {
                        $this->checkSeats($match->getDriver());
                        $seat = new Seat($match->getDriver(), $drive->getUser());
                    }
                    $em->persist($seat);
                }

                $em->flush();

                $data = [
                    'data' => $match
                ];
                break;
            }
            
        }

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new JsonResponse($json, JsonResponse::HTTP_OK, $headers, true);
        return $response;
    }
}
