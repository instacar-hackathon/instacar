<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;

class UserController extends Controller
{
    /**
     * @Route("/me")
     * @Method({"OPTIONS", "GET"})
     */
    public function meAction(Request $request)
    {
        if ($request->getMethod() === 'OPTIONS') {
            $headers = [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
            ];
            $response = new JsonResponse([], JsonResponse::HTTP_OK, $headers);
            return $response;
        }
        $userId = $request->headers->get('Authorization');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        $data = [
            'data' => $user
        ];

        $json = $this->get('serializer')->serialize($data, 'json');
        $headers = [
            'Access-Control-Allow-Origin' => '*'
        ];
        $response = new JsonResponse($json, JsonResponse::HTTP_OK, $headers, true);
        return $response;
    }
}