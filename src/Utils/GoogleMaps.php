<?php

namespace App\Utils;

class GoogleMaps
        {
                // private $apikey;

                function __construct(){
                        $this->apikey = "AIzaSyCNEXunIfz8lpWPQSUZn-0G18nQkCcc020";
                }

                function getBestRoute($start, $destination, $passengers){
                        $ch = curl_init();
                        $routeInfo = new RouteInfo();
                
                        $waypoints = "";
                        $allWaypoints = array();
                        foreach ($passengers as $passenger){
                                $waypoints = $waypoints."|".$passenger->{'start'};
                                array_push($allWaypoints, $passenger->{'start'});
                                array_push($allWaypoints, $passenger->{'destination'});
                        }

                        $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&waypoints=optimize:true".$waypoints."&key=".$this->apikey;
                        
                        curl_setopt($ch, CURLOPT_URL, $apiurl); 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $json = json_decode(curl_exec($ch)); 
                          
                        
                        $passengersOrder = array();
                        foreach ($json->{'routes'}[0]->{'waypoint_order'} as $i){
                                array_push($passengersOrder, $passengers[$i]);
                        }
                        $routeInfo->passengersOrder = $passengersOrder;                 
                        
                        
                        $waypoints = "";
                        foreach ($allWaypoints as $waypoint)
                                $waypoints = $waypoints."|".$waypoint;
                        
                        $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&waypoints=optimize:true".$waypoints."&key=".$this->apikey;
                        
                
                        curl_setopt($ch, CURLOPT_URL, $apiurl); 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $json = json_decode(curl_exec($ch)); 
             
                        $waypointOrder = array();
                        foreach ($json->{'routes'}[0]->{'waypoint_order'} as $i){
                                array_push($waypointOrder, $allWaypoints[$i]);
                        }

                        $waypointOrder = array_unique($waypointOrder);

                        $routeInfo->waypointOrder = $waypointOrder;



                        $waypoints = "";
                        foreach ($waypointOrder as $waypoint)
                                $waypoints = $waypoints."|via:".$waypoint;
                        
                        $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&waypoints=optimize:true".$waypoints."&key=".$this->apikey;
                        
                
                        curl_setopt($ch, CURLOPT_URL, $apiurl); 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $json = json_decode(curl_exec($ch)); 
                        

                        $routeInfo->newTime = $json->{'routes'}[0]->{'legs'}[0]->{'duration'}->{'value'};
                        $routeInfo->newDistance = $json->{'routes'}[0]->{'legs'}[0]->{'distance'}->{'value'};



                        $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&key=".$this->apikey; 
                        curl_setopt($ch, CURLOPT_URL, $apiurl); 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $json = json_decode(curl_exec($ch)); 
                        $routeInfo->orgTime = $json->{'routes'}[0]->{'legs'}[0]->{'duration'}->{'value'};
                        $routeInfo->orgDistance = $json->{'routes'}[0]->{'legs'}[0]->{'distance'}->{'value'};
                        
                        $routeInfo->deltaTime = $routeInfo->newTime - $routeInfo->orgTime;
                        $routeInfo->deltaDistance = $routeInfo->newDistance - $routeInfo->orgDistance;
                        
                        $waypoints = "";
                        foreach($waypointOrder as $waypoint)
                                $waypoints  = $waypoints."/".$waypoint;
                        $routeInfo->googleURL = "https://www.google.de/maps/dir/".$start.$waypoints."/".$destination."/";
                        
                        $waypoints = "";
                        foreach ($waypointOrder as $waypoint)
                                $waypoints = $waypoints."|via:".$waypoint;
                        
                        $waypoints = ltrim($waypoints, '|');
                        $routeInfo->googleURLEmbeded = "https://www.google.com/maps/embed/v1/directions?origin=".$start."&destination=".$destination."&waypoints=".$waypoints."&key=".$this->apikey; 

                        var_dump($routeInfo);
                        curl_close($ch);         
                        
                        return $routeInfo;
                }


                function getRoute($start, $destination, $drive){
 
                        $ch = curl_init();
                        $routeInfo = new RouteInfo();
                        $waypoints = "|".$drive->getStartLocation()."|".$drive->getEndLocation();

                        $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&waypoints=optimize:true".$waypoints."&key=".$this->apikey; 
                        curl_setopt($ch, CURLOPT_URL, $apiurl); 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $json = json_decode(curl_exec($ch));

                        if ($json->{'routes'}[0]->{'waypoint_order'}[0] == 0){


                                $routeInfo->newTime = $json->{'routes'}[0]->{'legs'}[0]->{'duration'}->{'value'};
                                $routeInfo->newDistance = $json->{'routes'}[0]->{'legs'}[0]->{'distance'}->{'value'};
                                
                                
                                $apiurl = "https://maps.googleapis.com/maps/api/directions/json?origin=".$start."&destination=".$destination."&key=".$this->apikey; 
                                curl_setopt($ch, CURLOPT_URL, $apiurl); 
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                                $json = json_decode(curl_exec($ch));
                                curl_close($ch); 
                                $routeInfo->orgTime = $json->{'routes'}[0]->{'legs'}[0]->{'duration'}->{'value'};
                                $routeInfo->orgDistance = $json->{'routes'}[0]->{'legs'}[0]->{'distance'}->{'value'};
                                
                                $routeInfo->deltaTime = $routeInfo->newTime - $routeInfo->orgTime;
                                $routeInfo->deltaDistance = $routeInfo->newDistance - $routeInfo->orgDistance;
                                                        
                                $routeInfo->googleURL = "https://www.google.de/maps/dir/".$start."/".$drive->getStartLocation()."/".$drive->getEndLocation()."/".$destination."/";
                                
                                
                                return $routeInfo;
                        
                        } else {
                                curl_close($ch);
                        }
                        
                        

                        return null;
                }
        }