<?php

namespace App\Utils;

class RouteInfo
{
    public $orgTime;
    public $newTime;
    public $orgDistance;
    public $newDistance;
    public $deltaTime;
    public $deltaDistance;
    public $googleURL;
    public $passengersOrder;
    public $waypointOrder;
    public $googleURLEmbeded;
}
