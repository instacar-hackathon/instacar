<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="drive_request",uniqueConstraints={@ORM\UniqueConstraint(name="one_match_request_idx", columns={"driver_drive_id", "passenger_drive_id"})})
 */
class DriveRequest
{
    public function __construct($driver, $passenger)
    { 
        $this->driver = $driver;
        $this->passenger = $passenger;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Drive", inversedBy="drivers")
     * @ORM\JoinColumn(name="driver_drive_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="Drive", inversedBy="passengers")
     * @ORM\JoinColumn(name="passenger_drive_id", referencedColumnName="id")
     */
    private $passenger;

    /**
     * @ORM\Column(type="boolean")
     */
    private $driverChecked = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $passengerChecked = false;

    public function getId()
    {
        return $this->id;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function getPassenger()
    {
        return $this->passenger;
    }

    public function setDriverChecked()
    {
        $this->driverChecked = true;
    }

    public function setPassengerChecked()
    {
        $this->passengerChecked = true;
    }

    public function getDriverChecked()
    {
        return $this->driverChecked;
    }

    public function getPassengerChecked()
    {
        return $this->passengerChecked;
    }
}
