<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    public function __construct(
        $email,
        $name,
        $surname,
        $password,
        $image
    ) {
       $this->email = $email;
       $this->name = $name;
       $this->surname = $surname;
       $this->password = $password;
       $this->image = $image;
    }
  
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $surname;

    /**
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * One User has Many Addresses.
     * @ORM\OneToMany(targetEntity="Address", mappedBy="user")
     */
    private $addresses;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getAddresses()
    {
        return $this->addresses;
    }
}
