<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="seat")
 */
class Seat
{
    public function __construct($drive, $user)
    { 
        $this->drive = $drive;
        $this->user = $user;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Drive")
     * @ORM\JoinColumn(name="drive_id", referencedColumnName="id")
     */
    private $drive;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function getDrive()
    {
        return $this->drive;
    }

    public function getUser()
    {
        return $this->user;
    }
}
