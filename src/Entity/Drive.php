<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="drive", uniqueConstraints={@ORM\UniqueConstraint(name="one_open_drive_idx", columns={"user_id"}, options={"where": "(closed IS FALSE)"})})
 */
class Drive
{
    public function __construct(
        $user,
        $timestamp,
        $departure,
        $arrival,
        $minutesToStart,
        $timestampOfDeparture,
        $seating,
        $type,
        $status
    ) {
        $this->user = $user;
        $this->timestamp = $timestamp;
        $this->departure = $departure;
        $this->arrival = $arrival;
        $this->minutesToStart = $minutesToStart;
        $this->timestampOfDeparture = $timestampOfDeparture;
        $this->seating = $seating;
        $this->type = $type;
        $this->status = $status; 
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Addresses have One User.
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="departure_address_id", referencedColumnName="id")
     */
    private $departure;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="arrival_address_id", referencedColumnName="id")
     */
    private $arrival;

    /**
     * @ORM\Column(type="integer")
     */
    private $minutesToStart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestampOfDeparture;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $seating;

    /**
     * @ORM\ManyToOne(targetEntity="DriveType")
     * @ORM\JoinColumn(name="drive_type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="DriveStatus")
     * @ORM\JoinColumn(name="drive_status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * One User has Many Addresses.
     * @ORM\OneToMany(targetEntity="DriveRequest", mappedBy="driver")
     */
    private $drivers;

    /**
     * One User has Many Addresses.
     * @ORM\OneToMany(targetEntity="DriveRequest", mappedBy="passenger")
     */
    private $passengers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $closed = false;

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTimestampOfDeparture()
    {
        return $this->timestampOfDeparture;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function getArrival()
    {
        return $this->arrival;
    }

    public function getMinutesToStart()
    {
        return $this->minutesToStart;
    }

    public function getType()
    {
        return $this->type;
    }

    public function isDriver()
    {
        return $this->getType()->getId() === 10;
    }

    public function getSeating()
    {
        return $this->seating;
    }

    public function getStartLocation()
    {
        return $this->departure->getGoogleMapsLocation();
    }

    public function getEndLocation()
    {
        return $this->arrival->getGoogleMapsLocation();
    }

    public function setClosed()
    {
        $this->closed = true;
    }
}
