<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="address")
 */
class Address
{
    public function __construct($label, $city, $zipCode, $street, $user)
    {
        $this->label = $label;
        $this->city = $city;
        $this->zipCode = $zipCode;
        $this->street = $street;
        $this->user = $user;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string")
     */
    private $label;
    

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string")
     */
    private $street;

    /**
     * Many Addresses have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="addresses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getGoogleMapsLocation()
    {
        return urlencode(sprintf('%s,%s,+%d+%s', $this->street, $this->city, $this->zipCode, 'BZ'));
    }
}
