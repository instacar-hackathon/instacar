import axios from 'axios'

axios.interceptors.request.use(request => {
  request.baseURL = 'https://instacar.stefangasser.info'
  if (localStorage.getItem('user_id')) {
    request.headers.common['Authorization'] = localStorage.getItem('user_id')
  }

  return request
})
