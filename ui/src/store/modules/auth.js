import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  me: null
}

// mutations
export const mutations = {
  [types.ME] (state, data) {
    state.me = data
  }
}

// actions
export const actions = {
  async me ({ commit, dispatch }, payload) {
    const { data } = await axios.get('/me')

    commit(types.ME, data.data)
  }
}

// getters
export const getters = {
  me: state => state.me
}
