import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  drive: null,
  matches: []
}

// mutations
export const mutations = {
  [types.DRIVE] (state, drive) {
    state.drive = drive
  },

  [types.DRIVE_MATCHES] (state, matches) {
    state.matches = matches
  },

  [types.UPDATE_MATCHES] (state, data) {
    state.matches.forEach((match, key) => {
      if (data.type === 'passenger') {
        if (match.passenger.user.id === data.userId) {
          state.matches.splice(key, 1, data.data)
        }
      } else {
        if (match.driver.user.id === data.userId) {
          state.matches.splice(key, 1, data.data)
        }
      }
    })
  }
}

// actions
export const actions = {
  drive ({ commit, dispatch }, drive) {
    commit(types.DRIVE, drive)
  },

  async matches ({ commit, dispatch }, payload) {
    const data = await axios.get('/drive/matches')

    commit(types.DRIVE_MATCHES, data.data.data)
  },

  updateMatch ({ commit, dispatch }, payload) {
    console.log('update match')
    console.log(payload)

    commit(types.UPDATE_MATCHES, payload)
  }
}

// getters
export const getters = {
  drive: state => state.drive,
  matches: state => state.matches
}
