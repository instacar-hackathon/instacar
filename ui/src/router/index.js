import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import DriverList from '@/pages/DriverList'
import PassengerList from '@/pages/PassengerList'
import DriverMatch from '@/pages/DriverMatch'
import PassengerMatch from '@/pages/PassengerMatch'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/driver/list',
      name: 'driver-list',
      component: DriverList
    },
    {
      path: '/passenger/list',
      name: 'passenger-list',
      component: PassengerList
    },
    {
      path: '/driver/match',
      name: 'driver-match',
      component: DriverMatch
    },
    {
      path: '/passenger/match',
      name: 'passenger-match',
      component: PassengerMatch
    }
  ]
})
